# Calculates the covariance of Flask map simulations.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import time
import glob
import os

import numpy as np
import multiprocessing as mp
from natsort import natsorted

# ==========================================
# CONFIGURATIONS
# ==========================================
from configparser import ConfigParser 

# Loading config file 'esy.ini'.
# ini = './esy.ini'
ini = './esy.ini'
cfg = ConfigParser(inline_comment_prefixes="#")
cfg.read(ini)
config = dict(cfg.items('PARAMETERS'))


class Flask:
    def __init__(self, path, cores=1):
        self.cores = cores
        self.path = path
        self.bins = natsorted(glob.glob(path + '/z*'))

        self.bindatapath = []
        for k in range(len(self.bins)):
            self.bindatapath.append(glob.glob(self.bins[k]+"/Flask*.dat"))

        self.bined = 'Bined' in path.split('/')[-1] # Parametro para definir formato dos dados. Cl's binados não possuem cabeçalho no inicio do documento.
        
    def clMedia(self, binNum = int):
        """
        Calculates the average of the Power spectrum of the simulated Flask maps.
        
        output:  ..znzn/clmedio.dat
        """
        segundosi = time.time()

        # Opening files, skipping irrelevant lines with skiprows=skip.
        numberOfSimulations = len(self.bindatapath[binNum])
        for k in range(numberOfSimulations):
            # Defining 'skiprows' depending on the data set.
            if self.bined==True:
                skip = 0
            else:
                z = self.bindatapath[binNum][k].split("/")[-2].split('z')
                if z[1]==z[2]:
                    skip = 2
                else:
                    skip = 4

            if k==0:
                media = np.loadtxt(self.bindatapath[binNum][k],skiprows=skip)[:,1]/numberOfSimulations
            else:
                media = media + np.loadtxt(self.bindatapath[binNum][k],skiprows=skip)[:,1]/numberOfSimulations

            # Capturing values of l in the last round:
            if k==numberOfSimulations-1:
                l = np.loadtxt(self.bindatapath[binNum][k],skiprows=skip)[:,0]

        # Saving data
        mediaSalvar = np.concatenate([l.reshape((len(media),1)),media.reshape((len(media),1))],axis=1)

        tituloClMedio = "l and the average Cl's of the simulations."
        np.savetxt(str(self.bins[binNum]+'/clmedio.dat'), mediaSalvar, fmt=['%i','%.6e'], header=tituloClMedio)

        segundosf = time.time()
        print(str(int(100*binNum/len(self.bins))),"%",'finalizado')
        print('tempo[s]: ',format(segundosf-segundosi))



    def covariance(self, binNum = int): # binNum: bin number
        """
        Calculates the covariances of each redshift bin from simulated Flask maps.
        
        Output:
        Cov(bin_ij, bin_kl) for all i,j,k,l in ..covMatrix/...dat 
        
        (e.g.: ..covMatrix/z0z0-z0z0.dat, i,j,k,l = 0)
        """
        segundosi = time.time()

        # Creating directory to store data
        os.system(str('mkdir -p ' + self.path +'/covMatrix')) # mkdir -p: No erro if exists

        numberOfSimulations = len(self.bindatapath[binNum])

        path = self.bins[binNum]+'/clmedio.dat'
        media = np.loadtxt(path)[:,1]

        lenMedia = len(media)
        lenBins = len(self.bins)
        for bCross in range(binNum,lenBins):
            path = self.bins[bCross]+'/clmedio.dat'
            mediaCross = np.loadtxt(path)[:,1]

            covariancia = np.zeros((lenMedia,lenMedia)) # Square matrix
            for k in range(numberOfSimulations): # k-simulação 
                # Open Cl's
                if self.bined==True:
                    skip = 0
                else:
                    z = self.bindatapath[binNum][k].split("/")[-2].split('z')
                    if z[1]==z[2]:
                        skip = 2
                    else:
                        skip = 4
                cl = np.loadtxt(self.bindatapath[binNum][k],skiprows=skip)[:,1] # self.bindatapath[bin][simulation number][:,0=l,1=Cl]

                # Open clCross
                if self.bined==True:
                    skip = 0
                else:
                    z = self.bindatapath[bCross][k].split("/")[-2].split('z')
                    if z[1]==z[2]:
                        skip = 2
                    else:
                        skip = 4
                clCross = np.loadtxt(self.bindatapath[bCross][k],skiprows=skip)[:,1] # self.bindatapath[bin][simulation number][:,0=l,1=Cl]

                # Covariance
                for l in range(lenMedia): # Matrix row
                    for c in range(lenMedia): # Matrix column
                        covariancia[l,c] = covariancia[l,c] + (cl[l] - media[l])*(clCross[c] - mediaCross[c]) / (numberOfSimulations-1)

            # Saving data
            pathSave = self.path+'/covMatrix/'+self.bins[binNum].split("/")[-1]+'-'+self.bins[bCross].split("/")[-1]+'.dat'
            np.savetxt(pathSave, covariancia, fmt='%.5e')

        segundosf = time.time()
        print(str(int(100*binNum/len(self.bins))),"%",'finalizado')
        print('tempo[s]: ',format(segundosf-segundosi))


    ## By combining all the simulated covariance matrices into a single covariance matrix.
    def covMatrix(self):
        """
        Takes the covariance of specific bins and concatenates in the covariance matrix.
        pathInput = Flask/simulations/folder/path (string).
        output = Output/path
        """
        segundosi = time.time()

        bins = int(config['nbins'])

        count = 0
        covBinPaths = []
        for k in range(bins):
            for i in range(k,bins):
                tempPaths = glob.glob(self.path+"/covMatrix/z"+str(k)+'z'+str(i)+"-*.dat")
                covBinPaths.append(tempPaths)
                count = count + 1
        bins = count

        # Concatenating the elements of the symmetric triangular matrix.
        for i in range(bins): # Line
            for j in range(bins): # Columns
                if i<j:
                    covBin = np.loadtxt(covBinPaths[i][j-i]) # Aij
                else:
                    covBin = np.loadtxt(covBinPaths[j][i-j]) # Aji

                # Concatenating
                if j==0:
                    line = covBin
                else:
                    line = np.append(line,covBin,axis=1) 
            if i==0:
                matrizOut = line
            else:
                matrizOut = np.append(matrizOut,line,axis=0)


            print(str(int(100*(i+1)/bins)),"% Finalizado")

            segundosf = time.time()
            print('tempo[s]: ', format(segundosf-segundosi))

        np.savetxt(self.path+"/CovMatrix.dat", matrizOut, fmt='%.5e')


    def run(self):
        # Mean
        pool = mp.Pool(self.cores)
        for b in range(len(self.bins)):
            pool.apply_async(self.clMedia,args=(b,))
        pool.close()
        pool.join()

        # Covariance
        pool = mp.Pool(self.cores)
        for b in range(len(self.bins)):
            pool.apply_async(self.covariance,args=(b,))
        pool.close()
        pool.join()

        # Covariance Matrix
        self.covMatrix()
