# Delete n lines from Cl file.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import glob


def delheader(PATH='', NUM=0):
    """
    Delete n lines from Cl file.
    PATH = Folder of files.
    NUM = Number of lines to be deleted.
    """
    data = glob.glob(PATH)
    for k in range(NUM):
        for n in range(len(data)):
            file = open(data[n],'r+')
            lines = file.readlines()
            file.close()
            del lines[0]
            newfile = open(data[n],'w+')
            for line in lines:
                newfile.write(line)
            newfile.close()
