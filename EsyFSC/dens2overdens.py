# Convert fullsky Density_map.fits to Overdensity_map.fits
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import numpy as np
import healpy as hp


def dens2overdens(PATH='', NSIDE=512): #mapazeros
    """
    Convert fullsky Density_map.fits to Overdensity_map.fits

    PATH = './PATH/Density_map.fits'
    NSIDE= Maps resolution.

    Return: Write a Overdensity_map.fits.    
    """

    maploaded = hp.read_map(PATH) 
        
    c = np.where(maploaded>0)
    Density_cuted_map = np.zeros(12*NSIDE*NSIDE)
    Density_cuted_map[c] = maploaded[c]

    soma = np.sum(maploaded[c])
    
    mediaporpixel = soma/12/NSIDE/NSIDE

    overdensity = (Density_cuted_map - mediaporpixel)/mediaporpixel

    hp.write_map(PATH, overdensity, dtype=np.float32, overwrite=True)
    
#     return mediaporpixel
