import numpy as np
import glob
import time

## By combining all the simulated covariance matrices into a single covariance matrix.
def covMatrix(path,binI=0,binF=3):
    """
    Takes the covariance of specific bins and concatenates in the covariance matrix.
    pathInput = Flask/simulations/folder/path (string).
    output = Output/path
    """
    # segundosi = time.time() 

    binF = binF+1 # Just to adapt the notation used in the work
    covBinPaths = []
    for i in range(binI,binF):
        for j in range(i,binF):
            covPaths = []
            for k in range(i,binF):
                for l in range(k,binF):
                    tempPath = glob.glob(path+"/covMatrix/z"+str(i)+'z'+str(j)+"-z"+str(k)+"z"+str(l)+".dat")
                    if len(tempPath) > 0:
                        covPaths.append(tempPath[0])
            covBinPaths.append(covPaths)
            
    # Concatenating the elements of the symmetric triangular matrix.
    for i in range(len(covBinPaths)): # Line
        for j in range(len(covBinPaths)): # Columns
            if i<j:
                covBin = np.loadtxt(covBinPaths[i][j-i]) # Aij
            else:
                covBin = np.loadtxt(covBinPaths[j][i-j]) # Aji
    
            # Concatenating
            if j==0:
                line = covBin
            else:
                line = np.append(line,covBin,axis=1) 
        if i==0:
            matrizOut = line
        else:
            matrizOut = np.append(matrizOut,line,axis=0)
    
    
        print(str(int(100*(i+1)/len(covBinPaths))),"% Finalizado")
    
        # segundosf = time.time()
        # print('tempo[s]: ', format(segundosf-segundosi))
    
    np.savetxt(path+"/CovMatrix_bin"+str(binI)+"-bin"+str(binF-1)+".dat", matrizOut, fmt='%.5e')

if __name__ == "__main__":
    path = '/media/BINGODATA0/appCovAnalysis/data_Dados-tese/output/FLASKmaps/OverdensityBined9'
    binInicial = 0
    binFinal = 3
    covMatrix(path,binInicial,binFinal)


# Shape of the cov matrix:
    
#            z0z0      z0z1      z1z1
# z0z0  z0z0-z0z0 z0z0-z0z1 z0z0-z1z1
# z0z1            z0z1-z0z1 z0z1-z1z1
# z1z1                      z1z1-z1z1