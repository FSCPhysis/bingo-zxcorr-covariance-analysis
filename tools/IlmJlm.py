# Parallelization of IlmJlm calculation using PseudoPower-pmotta-version.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO


import numpy as np
import multiprocessing as mp
import glob
import os
import matplotlib.pyplot as plt

nCore = 4
nSide = 512
lMin = 0
lMax = 10

mask = 'PATH/PseudoPower-pmotta-version/dados/mask/N512_footprint.fits'
pseudoPowerDir = 'PATH/PseudoPower/PseudoPower-pmotta-version'
outputDir = 'PATH/CovarianceAnalysis/data/output/ilmjlm'
outputPlot = "PATH/data/plot/output/IlmJlm.svg"

def ilmjlm(lMin=int,lMax=int,nSide=int,output=''):
    """
    IlmJlm calculations using PseudoPower.
    
    inputs:
    lMin= l-Minimun (int).
    lMax= l-Maximun (int).
    nSide= nSide resolution of the mask (int).
    nCore= n-cores to parallelization (int).
    output= Output path (string).

    return: IlmJlm in lMin-lMax interval.
    """
    comand = str(pseudoPowerDir + '/IlmJlm ' 
                 + ' -O ' + output
                 + ' -m ' + mask
                 + ' -N ' + str(nSide)
                 + ' -l ' + str(lMin) 
                 + ' -L ' + str(lMax) )
    os.system(comand)

def div(lMin=int,lMax=int):  
    """
    Division of lMin and lMax to be computed.
    
    inputs:
    lMin= l-Minimun (int).
    lMax= l-Maximun (int).

    Return: list of lMin-lMax.
    """
    
    lMinlMax = []
    while int(lMax-lMin) > 1:
        if lMax>1:
            lMinlMax.append([lMin,(lMin + int((lMax-lMin)/2))])
        else:
            lMinlMax.append([lMin,(lMin + int(lMax))])
            
        if lMax<2:
            lMin = lMin+lMax
        else:
            lMin = lMin+int((lMax-lMin)/2)

    return lMinlMax

def plot(lMin=int, lMax=int, output=outputPlot):
    """
    Ploting lMin-lMax division.
    
    Return: Saving graph in output path.
    
    """
    lMinlMax = div(lMin,lMax)

    lminString = []
    for k in range(len(lMinlMax)):
        lminString.append(str(lMinlMax[k][0]))

    lmin = []
    for k in range(len(lMinlMax)):
        lmin.append(lMinlMax[k][0])
        
    lmax = []
    for k in range(len(lMinlMax)):
        lmax.append(lMinlMax[k][1])


    fig = plt.figure(figsize = (10,10))
    plt.rcParams['xtick.labelsize'] = 20
    plt.rcParams['ytick.labelsize'] = 20
    plt.rcParams["legend.fontsize"] = 20

    # plt.title('Delta L',size=20)
    plt.bar(lminString, np.array(lmax)-np.array(lmin))

    plt.xlabel(r'$\ell_{min}$',size=30)
    plt.ylabel(r'$\Delta \ell$',size=30)
    # plt.legend(fontsize=20)

    # Saving
    plt.savefig(output)

def run(lMin=lMin,lMax=lMax,nSide=nSide,nCore=nCore ,output=outputDir):
    """
    Run function.

    input:
    lMin= l-Minimun (int).
    lMax= l-Maximun (int).
    nSide= nSide resolution of the mask (int).
    nCore= n-cores to parallelization (int).
    output= Output path (string).

    Return: IlmJlm.dat
    """
    # Dividing the l intervals:
    lMinlMax = div(lMin,lMax)
    
    # Calculating the intervals of IlmJlm:
    pool = mp.Pool(nCore) # Parallelization. Open a processing pool with 'nCores'. 'nCores' = integer
    for k in range(len(lMinlMax)):
        pool.apply_async(ilmjlm, args=(lMinlMax[k][0], lMinlMax[k][1], nSide, str(outputDir + '/IlmJlm-'+str(k)+'.dat') ) )

    pool.close()
    pool.join()
    
    files = sorted(glob.glob(outputDir + '/IlmJlm-'+"*"))
    for k in range(len(files)):
        comand = str('cat ' + files[k]+" >> " + outputDir + '/IlmJlm.dat')
        os.system(comand)

    os.system('rm '+ outputDir + '/IlmJlm-' +"*")
    
    
if __name__ == "__main__":
    # run(lMin=int,lMax=int,nSide=int,nCore=1,output='')
    run() 










